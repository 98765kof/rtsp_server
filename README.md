# rtsp服务器

#### 项目介绍
1.图像采集（opencv）

2.图像编解码(h264或者其它)

3.数据传输(rtp)

4.web前端播放(vlc.js之类的播放器插件)

5.rtsp服务端（使用mongoose做网络传输框架）

6.web服务端


#### 软件架构
各模块都可独立运行与测试


#### 安装教程

1. mkdir build && cd build
2. cmake ..
3. make

#### 使用说明

1. 安装opencv
2. 编译工程
3. 运行服务端
4. 打开浏览器播放

#### 参与贡献


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)