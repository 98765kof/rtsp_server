#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc,char **argv)
{
	IplImage* pFrame =NULL;
	CvCapture *pCapture=cvCreateCameraCapture(0);
	time_t now,pre;
	time(&now);
	pre=now;
	if(!pCapture)
	{
		perror("open fail:");
		return -1;
	}
	pFrame=cvQueryFrame(pCapture);
	int count=0;
	float fps=cvGetCaptureProperty(pCapture,CV_CAP_PROP_FPS);
	int w=cvGetCaptureProperty(pCapture,CV_CAP_PROP_FRAME_WIDTH);
	int h=cvGetCaptureProperty(pCapture,CV_CAP_PROP_FRAME_HEIGHT);
	int codec=cvGetCaptureProperty(pCapture,CV_CAP_PROP_FOURCC);
	printf("width=%d,height=%d,fps=%f,codec=%d\n",w,h,fps,codec);

	while(1)
	{
		time(&now);
		pFrame=cvQueryFrame(pCapture);
		if(!pFrame)
		{
			usleep(10000);
			continue;
		}
		count++;
		if(abs(now-pre)>5)
		{
			pre=now;
			printf("query frame count=%d\n",count);
			count=0;
		}
		
	}
	cvReleaseCapture(&pCapture);
	
}
