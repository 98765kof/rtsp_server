#ifndef LOGPROC_H
#define LOGPROC_H 
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <stdarg.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/timeb.h>

#define APP_LOG_MAX_SIZE 500*1024
#define MAXLINELEN 40960
char *getTimeStr(char *buf,time_t t);
char *getDateStr(char *buf,time_t t);
void setDataLogPath(char *path);
char * getDataLogPath();
void setThreadName(char *name);
void checkAndClearLog(char *head,int count);
void writeLog(char *logfile,char *header,char *data,int maxsize);
void Log_Prn( const char *fmt, va_list ap, char *logfile ,int maxSize);
void AppLogOut(const char *fmt, ...);
void AlarmLogOut(const char *fmt, ...);
void FaultLogOut(const char *fmt, ...);
void RunEventLogOut(const char *fmt,...);
void DealLogOut(const char *fmt, ...);
void PropertyLogOut(const char *fmt, ...);
void MemLogOut(const char *fmt, ...);
void DebugLogOut(const char *fmt, ...);
void ControlLogOut(const char *fmt, ...);
#endif
